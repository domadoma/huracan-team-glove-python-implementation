Here is the python implementation of the Global Vectors (GloVe model) for Word Representation.
The implementation is easy enough, but for working we need the trained GloVe model (which has size ~350 mb).
